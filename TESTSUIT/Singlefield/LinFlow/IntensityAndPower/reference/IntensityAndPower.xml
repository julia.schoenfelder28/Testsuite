<?xml version="1.0"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../../share/xml/CFS-Simulation/CFS.xsd"
  xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title>LinFlow intensity and power test - reference</title>
    <authors>
      <author>Dominik Mayrhofer</author>
    </authors>
    <date>2023-08-17</date>
    <keywords>
      <keyword>flow</keyword>
    </keywords>
    <references></references>
    <isVerified>no</isVerified>
    <description>
        This test consists of a channel where a wave propagates through. Due to the viscosity in the medium, some energy is dissipated.
        We evaluate the intensity, surafce intensity, normal surface intensity as well as the power passin through a surface.
        The pressure only versions of these results are used to verify that a difference is present.
        The dissipated energy in a certain region has to be equal to the energy difference passing through the boundaries, which is verified in the reference (very fine simulation).
        All results have been manually checked and verified.
    </description>
  </documentation>
  <fileFormats>
    <input>
      <!--<cdb fileName="Channel.cdb"/>-->
      <hdf5 fileName="IntensityAndPower.h5ref"/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">
  	<variableList>
      <var name="sf" value="25"/>
      <var name="dt1" value="2.5e-4/sf"/>
      <var name="steps" value="30*sf"/>
  	  <var name="f_exc" value="100"/>
    </variableList>
  
    <regionList>
      <region name="Channel_1" material="FluidMat"/>
      <region name="Channel_2" material="FluidMat"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="Exc_LF"/>
      <surfRegion name="Slip_Bot"/>
      <surfRegion name="Slip_Top"/>
      <surfRegion name="NoPenetration_End"/>
      <surfRegion name="IF"/>
    </surfRegionList>
  </domain>

  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="orderVel">
      <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="orderPres">
      <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>
  
  <integrationSchemeList>
    <scheme id="integVel">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="integPres">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  

  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>steps</numSteps>
        <deltaT>dt1</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <fluidMechLin formulation="compressible" presPolyId="orderPres" velPolyId="orderVel" presIntegId="integPres" velIntegId="integVel">
        <regionList>
          <region name="Channel_1"/>
          <region name="Channel_2"/>
        </regionList>
        
        <bcsAndLoads> 
          <velocity name="Exc_LF">
            <comp dof="x" value="(t lt 1/(2*f_exc))? (1-cos(2*pi*f_exc*t)^2)^2*sin(2*pi*f_exc*t) : 0" />
          </velocity>
          <velocity name="Slip_Top">
            <comp dof="y" value="0" />
          </velocity>
          <velocity name="Slip_Bot">
            <comp dof="y" value="0" />
          </velocity>
          <velocity name="NoPenetration_End">
            <comp dof="x" value="0" />
          </velocity>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions outputIds="h5"/>        
          </nodeResult>
          <elemResult type="fluidMechIntensityPressureOnly">
            <allRegions outputIds="h5"/>
          </elemResult>
          <elemResult type="fluidMechIntensity">
            <allRegions outputIds="h5"/>
          </elemResult>
          <surfElemResult type="fluidMechSurfaceIntensityPressureOnly">
            <surfRegionList>
              <surfRegion name="Exc_LF" outputIds="h5,txt"/>
              <surfRegion name="IF" outputIds="h5,txt"/>
            </surfRegionList>
          </surfElemResult>
          <surfElemResult type="fluidMechSurfaceIntensity">
            <surfRegionList>
              <surfRegion name="Exc_LF" outputIds="h5,txt"/>
              <surfRegion name="IF" outputIds="h5,txt"/>
            </surfRegionList>
          </surfElemResult>
          <surfElemResult type="fluidMechNormalIntensityPressureOnly">
            <surfRegionList>
              <surfRegion name="Exc_LF" outputIds="h5,txt"/>
              <surfRegion name="IF" outputIds="h5,txt"/>
            </surfRegionList>
          </surfElemResult>
          <surfElemResult type="fluidMechNormalIntensity">
            <surfRegionList>
              <surfRegion name="Exc_LF" outputIds="h5,txt"/>
              <surfRegion name="IF" outputIds="h5,txt"/>
            </surfRegionList>
          </surfElemResult>
          <surfRegionResult type="fluidMechPowerPressureOnly">
            <surfRegionList>
              <surfRegion name="Exc_LF" outputIds="h5,txt"/>
              <surfRegion name="IF" outputIds="h5,txt"/>
            </surfRegionList>
          </surfRegionResult>
          <surfRegionResult type="fluidMechPower">
            <surfRegionList>
              <surfRegion name="Exc_LF" outputIds="h5,txt"/>
              <surfRegion name="IF" outputIds="h5,txt"/>
            </surfRegionList>
          </surfRegionResult>
          <regionResult type="fluidMechViscousDissipation">
            <allRegions outputIds="h5,txt"/>
          </regionResult>
        </storeResults>
      </fluidMechLin>
    </pdeList>
    
    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
