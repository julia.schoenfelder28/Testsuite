<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title>heat2D_L2Norm</title>
    <authors>
      <author>dmayrhof</author>
    </authors>
    <date>2022-08-07</date>
    <keywords>
      <keyword>smooth</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> 
      Non Just functionality test
    </references>
    <isVerified>yes</isVerified>
    <description>
      Static excitation of a chamber on two sides with Dirichlet BCs and zero flux on the others. We evaluate the L2Norm postprocessing result which uses the integrated function instead of the nodal L2 norm. We compare four cases, where we test either correct or too low integration order as well as the computation via only the PDE-result or only the reference solution.
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="heat2D_L2Norm.h5ref"/>      
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">
  	<variableList>
      <var name="d" value="1e-1"/>
  	  <var name="f_exc" value="100"/>
  	  <var name="amp" value="1"/>
    </variableList>
  
    <regionList>
      <region name="Chamber" material="iron"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="C_Exc"/>
      <surfRegion name="C_Side"/>
      <surfRegion name="C_Fix"/>
    </surfRegionList>
  </domain>

  <sequenceStep index="1"> <!-- Correct integration order, 0 solution reference -->
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <heatConduction>
        <regionList>
          <region name="Chamber"/> 
        </regionList>

        <bcsAndLoads>
          <temperature name="C_Exc" value="amp"/>
          <temperature name="C_Fix" value="0"/>
          <heatFlux name="C_Side" value="0"/>         
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions outputIds="h5" postProcId="L2"/>
          </nodeResult>
        </storeResults>
      </heatConduction>
    </pdeList>
    
    <postProcList>
      <postProc id="L2">
        <L2Norm resultName="L2" outputIds="txt,h5" integrationOrder="2" mode="absolute">
          <dof name="" realFunc="0" />
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="2"> <!-- Correct integration order, real solution reference, no excitation -->
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <heatConduction>
        <regionList>
          <region name="Chamber"/> 
        </regionList>

        <bcsAndLoads>
          <temperature name="C_Exc" value="0"/>
          <temperature name="C_Fix" value="0"/>
          <heatFlux name="C_Side" value="0"/>         
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions outputIds="h5" postProcId="L2sol"/>
          </nodeResult>
        </storeResults>
      </heatConduction>
    </pdeList>
    
    <postProcList>
      <postProc id="L2sol">
        <L2Norm resultName="L2sol" outputIds="txt,h5" integrationOrder="2" mode="absolute">
          <dof name="" realFunc="amp*y/d"/>
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="3"> <!-- Integration order too low, 0 solution reference -->
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <heatConduction>
        <regionList>
          <region name="Chamber"/> 
        </regionList>

        <bcsAndLoads>
          <temperature name="C_Exc" value="amp"/>
          <temperature name="C_Fix" value="0"/>
          <heatFlux name="C_Side" value="0"/>         
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions outputIds="h5" postProcId="L2low"/>
          </nodeResult>
        </storeResults>
      </heatConduction>
    </pdeList>

    <postProcList>
      <postProc id="L2low">
        <L2Norm resultName="L2low" outputIds="txt,h5" integrationOrder="1" mode="absolute">
          <dof name="" realFunc="0" />
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="4"> <!-- Integration order too low, real solution reference, no excitation -->
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <heatConduction>
        <regionList>
          <region name="Chamber"/> 
        </regionList>

        <bcsAndLoads>
          <temperature name="C_Exc" value="0"/>
          <temperature name="C_Fix" value="0"/>
          <heatFlux name="C_Side" value="0"/>         
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions outputIds="h5" postProcId="L2solLow"/>
          </nodeResult>
        </storeResults>
      </heatConduction>
    </pdeList>
    
    <postProcList>
      <postProc id="L2solLow">
        <L2Norm resultName="L2solLow" outputIds="txt,h5" integrationOrder="1" mode="absolute">
          <dof name="" realFunc="amp*y/d"/>
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="5"> <!-- Relative L2Norm: Correct integration order, 0 solution reference -->
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <heatConduction>
        <regionList>
          <region name="Chamber"/> 
        </regionList>

        <bcsAndLoads>
          <temperature name="C_Exc" value="amp"/>
          <temperature name="C_Fix" value="0"/>
          <heatFlux name="C_Side" value="0"/>         
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions outputIds="h5" postProcId="L2rel"/>
          </nodeResult>
        </storeResults>
      </heatConduction>
    </pdeList>
    
    <postProcList>
      <postProc id="L2rel">
        <L2Norm resultName="L2rel" outputIds="txt,h5" integrationOrder="2" mode="relative">
          <dof name="" realFunc="0" />
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="6"> <!-- Relative L2Norm: Correct integration order, real solution reference, no excitation -->
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <heatConduction>
        <regionList>
          <region name="Chamber"/> 
        </regionList>

        <bcsAndLoads>
          <temperature name="C_Exc" value="0"/>
          <temperature name="C_Fix" value="0"/>
          <heatFlux name="C_Side" value="0"/>         
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions outputIds="h5" postProcId="L2relSol"/>
          </nodeResult>
        </storeResults>
      </heatConduction>
    </pdeList>
    
    <postProcList>
      <postProc id="L2relSol">
        <L2Norm resultName="L2relSol" outputIds="txt,h5" integrationOrder="2" mode="relative">
          <dof name="" realFunc="amp*y/d"/>
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="7"> <!-- Relative L2Norm: Integration order too low, 0 solution reference -->
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <heatConduction>
        <regionList>
          <region name="Chamber"/> 
        </regionList>

        <bcsAndLoads>
          <temperature name="C_Exc" value="amp"/>
          <temperature name="C_Fix" value="0"/>
          <heatFlux name="C_Side" value="0"/>         
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions outputIds="h5" postProcId="L2relLow"/>
          </nodeResult>
        </storeResults>
      </heatConduction>
    </pdeList>

    <postProcList>
      <postProc id="L2relLow">
        <L2Norm resultName="L2relLow" outputIds="txt,h5" integrationOrder="1" mode="relative">
          <dof name="" realFunc="0" />
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

  <sequenceStep index="8"> <!-- Relative L2Norm: Integration order too low, real solution reference, no excitation -->
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <heatConduction>
        <regionList>
          <region name="Chamber"/> 
        </regionList>

        <bcsAndLoads>
          <temperature name="C_Exc" value="0"/>
          <temperature name="C_Fix" value="0"/>
          <heatFlux name="C_Side" value="0"/>         
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions outputIds="h5" postProcId="L2relSolLow"/>
          </nodeResult>
        </storeResults>
      </heatConduction>
    </pdeList>
    
    <postProcList>
      <postProc id="L2relSolLow">
        <L2Norm resultName="L2relSolLow" outputIds="txt,h5" integrationOrder="1" mode="relative">
          <dof name="" realFunc="amp*y/d"/>
        </L2Norm>
      </postProc>
    </postProcList>
  </sequenceStep>

</cfsSimulation>
