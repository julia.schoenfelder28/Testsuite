<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <documentation>
        <title>Simple Current Sheet</title>
        <authors>
            <author>ftoth</author>
        </authors>
        <date>2018-09-15</date>
        <keywords>
            <keyword>magneticEdge</keyword>
        </keywords>
        <references>http://web.mit.edu/viz/EM/visualizations/coursenotes/modules/guide09.pdf</references>
        <isVerified>no</isVerified>
        <description> We condiser an infinite current sheet with uniform current density J in a
            unit-cube. The current density is applied in y direction, thus we assume flux-parallel
            on the boundaries parallel to the x-z plane (S_S and S_N, south and north). Furthermore,
            we set fluxParallel on the top, x-y parallel plane (S_T), and leave the bottom plane
            free. This implies B=0 at the bottom face z=0 (S_B) and is, thus, a symmetry BC. The H
            field is then linearly distributed as H(z) = J*z Thus, we can test any non-linear
            BH-curve simply by looking at the resulting B-field: Assume B = sqrt(H), we get nu = H/B
            = B^2/B = B, and nu' = 1, which is speciefied in the material definition. For a
            comparison with the analysic solution see the iPython notebook. </description>
    </documentation>
    <fileFormats>
        <input>
            <!--<hdf5 fileName="CurrentSheet3D.h5ref"/>
            -->            
            <gmsh fileName="UnitCube.msh"/>
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>
    <domain geometryType="3d" printGridInfo="no">
        <variableList>
            <var name="J" value="1.0"/>
            <var name="f_coil" value="5"/>
        </variableList>
        <regionList>
            <region name="V" material="analytic-exp"/>
        </regionList>
        <surfRegionList> </surfRegionList>
    </domain>

    <fePolynomialList>
        <Legendre id="Hcurl">
            <isoOrder>0</isoOrder>
        </Legendre>
    </fePolynomialList>

    <sequenceStep index="1">
        <analysis>
            <multiharmonic>
                <baseFreq>f_coil</baseFreq>
                <numHarmonics_N>11</numHarmonics_N>
                <numFFTPoints>1024</numFFTPoints>
            </multiharmonic>
        </analysis>

        <pdeList>
            <magneticEdge>
                <regionList>
                    <region name="V" polyId="Hcurl" nonLinIds="perm"/>
                </regionList>

                <nonLinList>
                    <permeability id="perm"/>
                </nonLinList>

                <bcsAndLoads>
                    <fluxParallel name="S_S"/>
                    <fluxParallel name="S_N"/>
                    <fluxParallel name="S_T"/>
                </bcsAndLoads>

                <coilList>
                    <coil id="coil">
                        <source type="current" value="J*sqrt(2*pi*f_coil)/2"/>
                        <part id="1">
                            <regionList>
                                <region name="V"/>
                            </regionList>
                            <direction>
                                <analytic>
                                    <comp dof="y" value="1"/>
                                </analytic>
                            </direction>
                            <wireCrossSection area="1.0"/>
                        </part>
                    </coil>
                </coilList>

                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magTotalCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magFieldIntensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>                    
                    <!--<elemResult type="magElemPermeability">
                        <allRegions/>
                    </elemResult>-->
                    <!--
                    <elemResult type="magCoilCurrentDensity">
                        <allRegions/>
                    </elemResult>-->
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                </storeResults>
            </magneticEdge>


        </pdeList>
        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                        <exportLinSys mass="true" stiffness="true" system="true"/>
                        <matrix reordering="noReordering" storage="sparseNonSym"/>
                        <nonLinear method="fixPoint">
                            <lineSearch/>
                            <incStopCrit>1e-5</incStopCrit>
                            <resStopCrit>1e-5</resStopCrit>
                            <maxNumIters>20</maxNumIters>
                        </nonLinear>
                    </standard>
                </solutionStrategy>
                <solverList>
                    <pardiso id="pardiso"/>
                    <gmres>
                        <stoppingRule type="absNorm"/>
                        <tol>1e-16</tol>
                        <maxIter>15</maxIter>
                        <maxKrylovDim>11</maxKrylovDim>
                        <logging>yes</logging>
                        <consoleConvergence>yes</consoleConvergence>
                    </gmres>
                </solverList>
                <precondList>
                    <SBMDiag>
                        <precond block="1" id="def"/>
                        <precond block="2" id="def"/>
                        <precond block="3" id="def"/>
                        <precond block="4" id="def"/>
                        <precond block="5" id="def"/>
                        <precond block="6" id="def"/>
                        <precond block="7" id="def"/>
                        <precond block="8" id="def"/>
                        <precond block="9" id="def"/>
                        <precond block="10" id="def"/>
                        <precond block="11" id="def"/>
                        <precond block="12" id="def"/>
                        <precond block="13" id="def"/>
                        <precond block="14" id="def"/>
                        <precond block="15" id="def"/>
                        <precond block="16" id="def"/>
                        <precond block="17" id="def"/>
                        <precond block="18" id="def"/>
                        <precond block="19" id="def"/>
                        <precond block="20" id="def"/>
                        <precond block="21" id="def"/>
                        <precond block="22" id="def"/>
                        <precond block="23" id="def"/>
                        <precond block="24" id="def"/>
                        <precond block="25" id="def"/>
                        <precond block="26" id="def"/>
                    </SBMDiag>
                    <pardiso id="def">
                        <type>direct</type>
                        <IterRefineSteps>20</IterRefineSteps>
                        <logging>no</logging>
                        <stats>no</stats>
                    </pardiso>
                </precondList>
            </system>
        </linearSystems>
    </sequenceStep>
</cfsSimulation>
