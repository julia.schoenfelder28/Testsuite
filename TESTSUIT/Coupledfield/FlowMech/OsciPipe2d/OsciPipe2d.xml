<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">

  <documentation>
    <title>Harmonic oscillatory tube for viscosity measurement - Couette Problem</title>
    <authors>
      <author>Simon Triebenbacher</author>
    </authors>
    <date>2012-03-09</date>
    <keywords>
      <keyword>CFD</keyword>
      <keyword>FluidMechPerturbedPDE</keyword>
    </keywords>
    <references>
    </references>
    <isVerified>no</isVerified>
    <description>
      c.f. theory.pdf, theory2.pdf, huber2006.pdf
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <!--<gmsh fileName="OsciPipe2d.msh"/>-->
      <hdf5 fileName="OsciPipe2d.h5ref"/>
    </input>
    <output>
      <hdf5 id="h5"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <variableList>
      <var name="R_a"  value="10e-3"/>
      <var name="R_i"  value="8.8e-3"/>
      <var name="Freq" value="725"/>
      <var name="D"    value="2*pi*R_a / 360"/>
      <var name="V"    value="D*2*pi*Freq"/>
    </variableList>
    <regionList>
      <region name="water" material="vivek"/>
      <region name="pipe" material="steel"/>
    </regionList>
    <nodeList>
      <nodes name="center">
        <coord x="0.0" y="0.0"/>
      </nodes>
    </nodeList>
  </domain>

  <fePolynomialList>
    <Legendre id="default">
      <isoOrder>2</isoOrder> 
    </Legendre>
    <Legendre id="velPolyId">
      <isoOrder>2</isoOrder> 
    </Legendre>
    
    <Legendre id="presPolyId">
      <isoOrder>1</isoOrder> 
    </Legendre>
  </fePolynomialList>
  
  <integrationSchemeList>
    <scheme id="default">
      <method>Gauss</method>
      <order>2</order>
      <mode>relative</mode>
    </scheme>

    <scheme id="velIntegId">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="presIntegId">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  
  
  <sequenceStep>
    <analysis>
      <harmonic>
        <numFreq>1</numFreq>
        <frequencyList>
          <freq value="Freq"/>
        </frequencyList>        
      </harmonic>
    </analysis>
      
    <pdeList>
      <fluidMech formulation="perturbed">
        <regionList>
          <region name="water"  polyId="velPolyId"/>
        </regionList>
        
        <bcsAndLoads>
          <noPressure name="center"/>
          
          <!--noPressure name="inner"/-->
          <!--noSlip name="inner">
            <comp dof="x"/>
            <comp dof="y"/>
          </noSlip-->
          <!--velocity name="outer">
            <comp dof="x" value="-1e-3*sin(atan2(y,x))"/>
            <comp dof="y" value=" 1e-3*cos(atan2(y,x))"/>
          </velocity-->
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions/>         
          </nodeResult>
          <elemResult type="fluidMechStress">
            <allRegions/>         
          </elemResult>
          <elemResult type="fluidMechStrainRate">
            <allRegions/>
          </elemResult>
          <sensorArray fileName="vel-line_new.csv" type="fluidMechVelocity" csv="yes" delimiter=",">
            <parametric>
              <list comp="x" start="0" stop="R_i" inc="1e-5"/>
              <list comp="y" start="0" stop="0" inc="0.1"/>
            </parametric>
          </sensorArray>
          <sensorArray fileName="strain-line_new.csv" type="fluidMechStrainRate" csv="yes" delimiter=",">
            <parametric>
              <list comp="x" start="0" stop="R_i" inc="1e-5"/>
              <list comp="y" start="0" stop="0" inc="0.1"/>
            </parametric>
          </sensorArray>
        </storeResults>
      </fluidMech>
      
      <mechanic subType="planeStrain">
        <regionList>
          <region name="pipe"/>
        </regionList>

        <bcsAndLoads>
          <!--traction name="pipe_inner">
            <comp dof="x" value=" 1000*sin(atan2(y,x))" phase="0"/>
            <comp dof="y" value="-1000*cos(atan2(y,x))" phase="0"/>
          </traction-->
          
          <displacement name="pipe_outer">
            <comp dof="x" value=" D*sin(atan2(y,x))" phase="0"/>
            <comp dof="y" value="-D*cos(atan2(y,x))" phase="0"/>
          </displacement>

          <!--displacement name="pipe_inner"-->
            <!-- Bei Anregung mit Phase=-90° wird ein Sinus eingeprägt. -->
            <!--comp dof="x" value=" D*sin(atan2(y,x))" phase="0"/>
            <comp dof="y" value="-D*cos(atan2(y,x))" phase="0"/>
            
          </displacement-->
          
        </bcsAndLoads>
          <storeResults>          
            <nodeResult type="mechDisplacement">
              <allRegions/>
            </nodeResult>
            <nodeResult type="mechVelocity">
              <allRegions/>
            </nodeResult>
            <elemResult type="mechStress">
              <allRegions/>            
            </elemResult>
            <sensorArray fileName="mech-stress-line_new.csv" type="mechStress" csv="yes" delimiter=",">
              <parametric>
                <list comp="x" start="R_i" stop="10e-3" inc="1e-5"/>
                <list comp="y" start="0" stop="0" inc="0.0"/>
              </parametric>
            </sensorArray>
            <sensorArray fileName="mech-vel-File_new.csv" type="mechVelocity" csv="yes" delimiter=",">
              <coordinateFile fileName="singleSensor.csv" xCoordColumn="1" yCoordColumn="2" zCoordColumn="3"/>
            </sensorArray>
          </storeResults>
        
      </mechanic>
    </pdeList>
    
    <couplingList>
      <direct>
        <fluidMechDirect lmOrderSameAsVel="false">
          <surfRegionList>
            <surfRegion name="pipe_inner"  polyId="velPolyId"/>
          </surfRegionList>
          <storeResults>
            <nodeResult type="lagrangeMultiplier">
              <regionList>
                <region name="pipe_inner"/>
              </regionList>              
            </nodeResult>
          </storeResults>

        </fluidMechDirect>
      </direct>      
    </couplingList>
    
    <linearSystems>
      <system>
        <solverList>
          <umfpack/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
