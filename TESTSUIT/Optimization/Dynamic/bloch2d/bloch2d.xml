<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>Maximization of Bloch band gap</title>
    <authors>
      <author>Fabian Wein</author>
    </authors>
    <date>2016-02-19</date>
    <keywords>
      <keyword>SIMP</keyword>
    </keywords>
    <references>Sigmund and Jensen; 2003</references>
    <isVerified>no</isVerified>
    <description>For other sizes Arpack might fail and works with continuation/ restart. Note that MKL does not work, therefore no Pardiso. Does not work with homogeneous initial design.
    Initial design to be created by create_density.py, mesh create by create_mesh.py and design shown by show_density.py. 
    Formulation from Michael.
    </description>
  </documentation>

  <fileFormats>
    <output>
      <hdf5 />
      <info/>
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region material="99lines" name="mech" />
    </regionList>
    <nodeList>
      <nodes name="center">
        <coord x="0.5" y="0.5" />
      </nodes>
    </nodeList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <eigenFrequency sort="true">
        <isQuadratic>no</isQuadratic>
        <numModes>9</numModes>
        <freqShift>0</freqShift>
        <writeModes>no</writeModes>
        <bloch>
           <ibz sample="symmetric" steps="6" />
        </bloch>
      </eigenFrequency>
    </analysis>

    <pdeList>
      <mechanic subType="planeStrain">
        <regionList>
          <region name="mech" />
        </regionList>

        <bcsAndLoads>
          <periodic master="north" slave="south" dof="x"  quantity="mechDisplacement" />
          <periodic master="north" slave="south" dof="y"  quantity="mechDisplacement" />
          <periodic master="west"  slave="east" dof="x"  quantity="mechDisplacement" />
          <periodic master="west"  slave="east" dof="y"  quantity="mechDisplacement" />
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions />
          </nodeResult>
          <elemResult type="mechPseudoDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="physicalPseudoDensity">
            <allRegions/>
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
          </standard>
        </solutionStrategy>
        <eigenSolverList>
          <arpack>
            <logging>false</logging>
          </arpack>
        </eigenSolverList>
        <solverList>
          <umfpack/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>


  <loadErsatzMaterial file="circular_2d-v_0.5-o_6-inv_10.density.xml" set="last"/>

  <optimization>
    <costFunction type="slack" task="maximize" multiple_excitation="true">
      <stopping queue="999" value="0.001" type="designChange"/>
    </costFunction>

    <constraint type="eigenfrequency" bound="upperBound" value="alpha-slack" ev="2" mode="constraint" bloch="full" />
    <constraint type="eigenfrequency" bound="upperBound" value="alpha-slack" ev="3" mode="constraint" bloch="full" />
    <constraint type="eigenfrequency" bound="lowerBound" value="alpha+slack" ev="4" mode="constraint" bloch="full" /> 
    <constraint type="eigenfrequency" bound="lowerBound" value="alpha+slack" ev="5" mode="constraint" bloch="full" />
    
    <constraint type="volume" bound="lowerBound" value="0.5" mode="constraint"  />
    
    <optimizer type="snopt" maxIterations="20">
      <snopt>
        <option type="integer" name="verify_level" value="-1"/>
      </snopt>
    </optimizer>
    
    <ersatzMaterial region="mech" material="mechanic" method="simp">
       <filters>
        <filter neighborhood="maxEdge" value="2.1" type="density"/>
       </filters>
      
      <design name="density" initial=".5" physical_lower="1e-6" upper="1.0" enforce_bounds="true"/>
      <design name="slack" initial="0" lower="0" upper="1e4"/>
      <design name="alpha" initial="1200" lower="0" upper="1e4"/>

      <transferFunction type="simp" param="3" application="mech" />
      <transferFunction type="simp" param="3" application="mass" />

      <export save="all" write="iteration"/>
      
    </ersatzMaterial>
    <commit mode="forward" stride="999"/>
  </optimization>
  


</cfsSimulation>


