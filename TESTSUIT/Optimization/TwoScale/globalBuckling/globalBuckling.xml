<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation" id="0">
  <documentation>
    <title>test for interpolation of globalized local buckling load factor</title>
    <authors>
      <author>Daniel Huebner</author>
    </authors>
    <date>2023-12-06</date>
    <keywords>
      <keyword>buckling</keyword>
    </keywords>
    <references></references>
    <isVerified>no</isVerified>
    <description>
      this is a globalized version of test localBuckling.
      the formula is sum_e max(parameter-lambda_e, 0)^power less than value,
      where e = 1, ..., N (number of FE)
      mesh created with 'create_mesh.py --res 5 --width 1 --type bulk2d --file globalBuckling.mesh'
    </description>
  </documentation>
  <fileFormats>
    <input>
      <hdf5 fileName="globalBuckling.h5ref"/>
    </input>
    <output>
      <hdf5 />
    </output>
    <materialData file="./mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region name="mech" material="strong" />
    </regionList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static />
    </analysis>
    <pdeList>
      <mechanic subType="planeStress">
        <regionList>
          <region name="mech"/>
        </regionList>
        <bcsAndLoads>
          <fix name="left">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <force name="bottom_right">
            <comp dof="y" value="-1"/>
          </force>
        </bcsAndLoads>
        <storeResults>
          <elemResult type="mechStress">
            <allRegions />
          </elemResult>
          <nodeResult type="mechRhsLoad">
            <allRegions />
          </nodeResult>
          <nodeResult type="mechDisplacement">
            <allRegions />
          </nodeResult>
          <elemResult type="optResult_1">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_2">
            <allRegions/>
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
  </sequenceStep>

  <optimization log="">
    <costFunction type="compliance" task="minimize" linear="false" >
      <stopping queue="5" value="1e-6" type="designChange"/>
    </costFunction>

    <!-- for a gradient check the parameter has to be set to a high value (due to the max function) -->
    <constraint type="globalBucklingLoadFactor" bound="upperBound" value="1e-6" mode="constraint" parameter=".06">
      <local power="1.0" normalize="false"/>
    </constraint>

    <constraint type="globalTwoScaleVolume" bound="upperBound" value="0.5" linear="true" mode="constraint">
      <local normalize="true" power="1" />
    </constraint>
    
    <optimizer type="snopt" maxIterations="5">
      <snopt>
        <option name="major_optimality_tolerance" type="real" value="1e-8"/>
        <option name="verify_level" type="integer" value="-1"/>
      </snopt>
    </optimizer>
      
    <ersatzMaterial material="mechanic" method="paramMat" region="mech">
      <designSpace local_element_cache="false"/>
      <filters>
        <filter neighborhood="maxEdge" value="1.6" type="density" design="stiff1"/>
      </filters>
      <paramMat>
        <designMaterials>
          <designMaterial type="hom-rect-C1" sequence="1">
            <param name="stiff2" value="0.0"/>
            <param name="rotAngle" value="0.0"/>
            <param name="shear1" value="0.5"/>
            <homRectC1 file="catalogue_cubit_diamond_b0.600_0.001000_r3.xml"/>
          </designMaterial>
        </designMaterials>
      </paramMat>
      <design name="stiff1" initial=".4" lower="0.1" upper="1.0" region="mech"/>
      <result value="design" design="stiff1" id="optResult_1" access="smart"/>
      <result value="localBucklingLoadFactor" id="optResult_2"/>
    </ersatzMaterial>
    <commit mode="forward" stride="1"/>
  </optimization>
  
</cfsSimulation>
